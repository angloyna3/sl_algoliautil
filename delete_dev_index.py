import sys, os
from algoliasearch import algoliasearch

def delete_dedicated_dotme_index(INDEX_NAME):
	try:
		SLAPI_ALGOLIA_APPLICATION_ID = os.environ['SLAPI_ALGOLIA_APPLICATION_ID']
		SLAPI_ALGOLIA_WRITE_API_KEY = os.environ['SLAPI_ALGOLIA_WRITE_API_KEY']

		client = algoliasearch.Client(SLAPI_ALGOLIA_APPLICATION_ID, SLAPI_ALGOLIA_WRITE_API_KEY)

		client.delete_index(INDEX_NAME)
	except algoliasearch.AlgoliaException as strerror:
		print "unable to delete algolia index"
		print strerror
