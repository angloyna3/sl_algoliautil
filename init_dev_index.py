import sys, os
from algoliasearch import algoliasearch

def create_dedicated_dotme_index(DEDICATED_INDEX_NAME, INDEX_TYPE):
	try:
		SLAPI_ALGOLIA_APPLICATION_ID = os.environ['SLAPI_ALGOLIA_APPLICATION_ID']
		SLAPI_ALGOLIA_WRITE_API_KEY = os.environ['SLAPI_ALGOLIA_WRITE_API_KEY']
		SLAPI_STAGE_ALGOLIA_INDEX = os.environ['SLAPI_STAGE_ALGOLIA_INDEX']

		client = algoliasearch.Client(SLAPI_ALGOLIA_APPLICATION_ID, SLAPI_ALGOLIA_WRITE_API_KEY)

		existingIndices = client.list_indexes()['items']

		duplicatedIndex = list(filter(lambda index: index['name'] == DEDICATED_INDEX_NAME, existingIndices))

		if duplicatedIndex:
			client.deleteIndex(map(lambda index: index['name'], duplicatedIndex)[0])

		if INDEX_TYPE == 'DEDICATED FULL':
			client.copy_index(SLAPI_STAGE_ALGOLIA_INDEX, DEDICATED_INDEX_NAME)
		elif INDEX_TYPE == 'DEDICATED PARTIAL':
			#this is where you pull 1K rows into the dedicated index
			stage_index = client.init_index(SLAPI_STAGE_ALGOLIA_INDEX)
			res = stage_index.browse('')
			dataset = res['hits']
			dotme_index = client.init_index(DEDICATED_INDEX_NAME)
			dotme_index.add_objects(dataset)
		else:
		    print >> sys.stderr "unable to create algolia index : index type not valid"
		    return False
		return True
	except algoliasearch.AlgoliaException as strerror:
		print >> sys.stderr "unable to create algolia index : " + strerror
		return False 
